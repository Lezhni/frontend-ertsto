$(document).ready(function() {

    $('#slider').owlCarousel({
        items: 1,
        itemsDesktop: false,
        itemsDesktopSmall: false,
        itemsTablet: false,
        itemsTabletSmall: false,
        itemsMobile: false,
        autoPlay: 6000,
        slideSpeed: 500,
        paginationSpeed: 500,
        navigation: true,
        pagination: false,
        theme: false,
        addClassActive: true,
    });
});